variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "Application name"
}

variable "domain" {
  type        = string
  description = "Domain name"
}

variable "deploy_domain" {
  type        = string
  description = "Project deploy domain"
}

variable "lb_name" {
  type        = string
  description = "General load balancer name"
}

variable "vpc_id" {
  type        = string
  description = "VPC identifier. Only required for external faced load balancer"
}

variable "healthcheck_path" {
  type        = string
  default     = "/ping"
  description = "Applcation healthcheck"
}

variable "healthcheck_success_code" {
  type        = string
  default     = "200"
  description = "Applcation healthcheck success code"
}

variable "path" {
  type        = string
  default     = "*"
  description = "Applcation path"
}

variable "port" {
  type        = number
  default     = 80
  description = "Applcation port"
}

