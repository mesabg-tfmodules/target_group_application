resource "random_string" "target_group_prefix" {
  length           = 6
  special          = false
}

resource "aws_lb_target_group" "lb_target_group_application" {
  name_prefix   = random_string.target_group_prefix.result
  port          = var.port
  protocol      = "HTTP"
  vpc_id        = var.vpc_id

  health_check {
    enabled     = true
    path        = var.healthcheck_path
    port        = "traffic-port"
    protocol    = "HTTP"
    matcher     = var.healthcheck_success_code
  }

  tags = {
    Name        = "${var.name}-${var.environment}"
    Environment = var.environment
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_rule" "lb_listener_rule_https" {
  listener_arn        = data.aws_lb_listener.lb_listener_https_application.arn

  condition {
    host_header {
      values          = [ var.deploy_domain ]
    }
  }

  condition {
    path_pattern {
      values          = [ var.path ]
    }
  }

  action {
    type              = "forward"
    target_group_arn  = aws_lb_target_group.lb_target_group_application.arn
  }

  lifecycle {
    create_before_destroy = true
  }
}
