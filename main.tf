terraform {
  required_version = ">= 0.13.2"
}

data "aws_acm_certificate" "certificate" {
  domain      = var.domain
  types       = ["AMAZON_ISSUED"]
  statuses    = ["ISSUED"]
  most_recent = true
}
