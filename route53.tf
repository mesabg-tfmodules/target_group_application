data "aws_route53_zone" "zone" {
  name = var.domain
}

resource "aws_route53_record" "route53_record" {
  zone_id                   = data.aws_route53_zone.zone.zone_id
  name                      = var.deploy_domain
  type                      = "A"

  alias {
    name                    = data.aws_lb.lb_application.dns_name
    zone_id                 = data.aws_lb.lb_application.zone_id
    evaluate_target_health  = false
  }
}
