output "target_group_arn" {
  value       = aws_lb_target_group.lb_target_group_application.arn
  description = "Created Target Group ARNs"
}
