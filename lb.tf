data "aws_lb" "lb_application" {
  name = var.lb_name
}

data "aws_lb_listener" "lb_listener_https_application" {
  load_balancer_arn = data.aws_lb.lb_application.arn
  port              = 443
}
