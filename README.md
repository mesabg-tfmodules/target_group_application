# Load Balancer Module

This module is capable to generate a target group and bind it to an existing application load balancer.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `domain` - base domain (only for external faced)
- `deploy_domain` - deploy domain (only for external faced)
- `lb_name` - load balancer name
- `vpc_id` - vpc identifier (only for external faced)
- `healthcheck_path` - healthcheck path (only for external faced, default /healthcheck)
- `path` - zone identifier (only for external faced)
- `port` - exposed port for containers (80)

Usage
-----

```hcl
module "target_group" {
  source                  = "git::https://gitlab.com/mesabg-tfmodules/target_group_application.git?ref=alb-r53/v2.0.0"

  environment             = "environment"
  name                    = "app_name"
  domain                  = "example.com"
  deploy_domain           = "some.example.com"

  lb_name                 = "my_load_balancer"
  vpc_id                  = "vpc_id"

  healthcheck_path        = "/ping"
  path                    = "*"
  port                    = 80
}
```

```hcl
module "target_group" {
  source                  = "git::https://gitlab.com/mesabg-tfmodules/target_group_application.git?ref=alb-r53/v2.0.0"

  environment             = "environment"
  name                    = "app_name"
  domain                  = "example.com"
  deploy_domain           = "some.example.com"

  lb_name                 = "my_load_balancer"
  vpc_id                  = "vpc_id"

  # healthcheck_path        = default value is "/ping"
  # path                    = default value is "*"
  # port                    = default value is 80
}
```

Outputs
=======

 - `target_group_arn` - Created Target Group resource ARN


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
